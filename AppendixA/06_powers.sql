INSERT INTO Power (name, description) VALUES
    ('Flight', 'Ability to fly'),
    ('Super Strength', 'Ability to lift very heavy objects'),
    ('Invisibility', 'Ability to become invisible'),
    ('Teleportation', 'Ability to move from one place to the other in 0-time and 0-space'),
    ('Web-shooting', 'Ability to shoot web from the hands'),
    ('Self-healing', 'Ability to heal cuts or wounds'),
    ('Inheritance of money', 'Ability to inherit a ton of money from parents / grandparents');

INSERT INTO powerherolink(superhero_id, power_id) VALUES
    (1, 1), -- Superman can fly
    (1, 2), -- Superman has super strength
    (2, 7), -- Bruce Wayne has the super power of inheriting money
    (3, 1), -- Wonder Woman can fly
    (3, 2), -- Wonder Woman has super strength
    (4, 5), -- Spiderman can shoot web
    (5, 4), -- Nightcrawler can teleport
    (6, 6); -- Wolverine can perform self-healing