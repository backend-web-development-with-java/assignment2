INSERT INTO Superhero (name, alias, origin) VALUES
    ('Clark Kent', 'Superman', 'Krypton'),
    ('Bruce Wayne', 'Batman', 'Gotham City'),
    ('Diana Prince', 'Wonder Woman', 'Themyscira'),
    ('Peter Parker', 'Spiderman', 'New York City'),
    ('Kurt Wagner', 'Nightcrawler', 'Bayern'),
    ('Logan Howlett', 'Wolverine', 'Cold Lake, CA');