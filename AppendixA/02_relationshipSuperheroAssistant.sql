
/* ADD A FOREIGN KEY THROUGH "ALTER" */
alter table assistant
add column superhero_id INTEGER REFERENCES superhero(id);

/* IF A HERO IS DELETED, DELETE THE CORRESPONDING HEROS AS WELL */
alter table assistant
add column superhero_id INTEGER REFERENCES superhero(id) on delete CASCADE;