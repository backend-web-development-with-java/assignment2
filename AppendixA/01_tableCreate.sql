
/* CREATE DATABASE */
CREATE DATABASE SuperHerosDb


/* CREATE THE SUPERHERO TABLE */
create table Superhero (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) not null,
    alias varchar(255) not null,
    origin varchar(255)
);


/* CREATE THE ASSISTANT TABLE */
CREATE TABLE Assistant (
  id serial primary key,
  name varchar(255) not null
);


/* CREATE THE POWER TABLE */
create table Power (
    id serial primary key,
    name varchar(255) not null,
    description varchar(255) not null
);

