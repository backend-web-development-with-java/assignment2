
/* CREATE THE LINKING TABLE */
CREATE TABLE PowerHeroLink(
    superhero_id INTEGER NOT NULL REFERENCES superhero(id),
    power_id INTEGER NOT NULL REFERENCES Power(id),
    PRIMARY KEY (superhero_id, power_id)
);

/* ALTER STATEMENT FOR ADDING CONSTRAINT FOR SUPERHERO FOREIGN KEY */
alter table PowerHeroLink
add constraint fk_superhero FOREIGN KEY (superhero_id) REFERENCES superhero(id);

/* ALTER STATEMENT FOR ADDING CONSTRAINT FOR POWER FOREIGN KEY */
alter table PowerHeroLink
add constraint fk_power FOREIGN KEY (power_id) REFERENCES Power(id);