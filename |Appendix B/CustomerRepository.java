package Repositories;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;



public interface CustomerRepository extends JpaRepository<Customer, Long> {

    // 1. return all customers
    public default List<Customer> getAllCustomers() {
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email " +
                "FROM Customer c";
        return jdbcTemplate.query(sql, (rs, rowNum) -> {
            Customer customer = new Customer();
            customer.setCustomer_id(rs.getLong("customer_id"));
            customer. (rs.getString("first_name"));
            customer.setLast_name(rs.getString("last_name"));
            customer.setCountry(rs.getString("country"));
            customer.setPostal_code(rs.getString("postal_code"));
            customer.setPhone(rs.getString("phone"));
            customer.setEmail(rs.getString("email"));
            return customer;
        });
    }


    // 2. read specific customer from the customer_id
    @Query("SELECT c FROM Customer c WHERE c.customer_id = :customerId")
    Customer findCustomerById(@Param("customerId") Long customerId);


    // 3. get customer by specific name
    public Optional<Customer> getCustomerByName(String name) {
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email " +
                "FROM Customer c " +
                "WHERE c.first_name LIKE ? OR c.last_name LIKE ?";
        Object[] params = {"%" + name + "%", "%" + name + "%"};
        List<Customer> customers = jdbcTemplate.query(sql, params, (rs, rowNum) -> {
            Customer customer = new Customer();
            customer.setCustomer_id(rs.getLong("customer_id"));
            customer.setFirst_name(rs.getString("first_name"));
            customer.setLast_name(rs.getString("last_name"));
            customer.setCountry(rs.getString("country"));
            customer.setPostal_code(rs.getString("postal_code"));
            customer.setPhone(rs.getString("phone"));
            customer.setEmail(rs.getString("email"));
            return customer;
        });
        return customers.isEmpty() ? Optional.empty() : Optional.of(customers.get(0));
    }

    // 4. return a page of customers from the database
    @Query(value = "SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email " +
            "FROM Customer c ORDER BY c.customer_id LIMIT :limit OFFSET :offset",
            nativeQuery = true)
    List<Customer> getPageOfCustomers(@Param("limit") int limit, @Param("offset") int offset);

    // 5. Add a new customer to the database
    @Transactional
    default Customer addCustomer(Customer customer) {
        return save(customer);
    }

    // 6. Update an existing customer
    Optional<Customer> customerOptional = customerRepository.findById(customerId);
    if(customerOptional.isPresent())
    {
        // update the customer properties
        Customer customer = customerOptional.get();
        customer.setFirst_name(first_name);
        customer.setLast_name(last_name);
        customer.setCountry(country);
        customer.setPostalCode(postalCode);
        customer.setPhone(phone);
        customer.setEmail(email);

        // save the updated customer to the database
        customerRepository.save(customer);
    }



    // 7. return the country with the most customers
    @Query(value = "SELECT c.country, COUNT(c.customer_id) as num_customers "
            + "FROM Customer c "
            + "GROUP BY c.country "
            + "ORDER BY num_customers DESC "
            + "LIMIT 1", nativeQuery = true)
    String findCountryWithMostCustomers();

    // 8. Return the highest spender
    @Query("SELECT c FROM Customer c JOIN c.invoices i GROUP BY c.customer_id ORDER BY SUM(i.total) DESC")
    List<Customer> findHighestSpender();


    // 9. Return the most popular genre for a given customer
    @Query("SELECT g.name AS genre, COUNT(*) AS count"
            + "FROM Customer c JOIN Invoice i ON c.customer_id = i.customer_id  JOIN InvoiceLine il ON i.invoice_id = il.invoice_id JOIN Track t ON il.track_id = t.track_id  JOIN Genre g ON t.genre_id = g.genre_id  WHERE c.customer_id = ?  GROUP BY g.genre_id  ORDER BY count DESC, g.name ASC  LIMIT 2")
    List<GenreCount> getCustomerTopGenres(long customerId, int limit);

}
