package Repositories;

import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CustomerService {

    // encapsuled field
    private final CustomerRepository customerRepository;


    // constructor method
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    // TODO: Throw in all methods from the interface

    // public methods form interface

    // get all customers
    public List getAllCustomers() {
        return customerRepository.getAllCustomers();
    }


    // get customer by name
    public List getcustomerByName() {
        return customerRepository.getCustomerByName();
    }


    // get customer by id
    public Customer findCustomerById(Long id) {
        return (Customer) findCustomerById().findCustomerById(id);
    }

    // get page of customers
    public void getPageOfCustomers (Customer customer) {
        customerRepository.getPageOfCustomers(customer);
    }

    // add customer
    public Customer addCustomer (Customer customer){
        return customerRepository.addCustomer(customer);
    }

    // 6. update an existing customer
    @Transactional
    public Customer updateCustomer(Long customerId, String firstName, String lastName, String country, String postalCode, String phone, String email) {
        Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();
            customer.setFirst_name(firstName);
            customer.setLast_name(lastName);
            customer.setCountry(country);
            customer.setPostalCode(postalCode);
            customer.setPhone(phone);
            customer.setEmail(email);
            return customerRepository.save(customer);
        } else {
            throw new RuntimeException("Customer not found with id " + customerId);
        }
    }

    // 7. Return the country with the most customers
    public String findCountryWithMostCustomers() {
        return customerRepository.findCountryWithMostCustomers();
    }

    // 8. Return the highest spender
    public List<Customer> findHighestSpender() {
        return customerRepository.findHighestSpender();
    }

    // 9. Return the most popular genre for a given customer
    public List<GenreCount> getCustomerTopGenres(long customerId, int limit) {
        return customerRepository.getCustomerTopGenres(customerId, limit);
    }


    // delete a customer
    public void deleteCustomer(Long id) {
        customerRepository.deleteById(id);
    }

}
